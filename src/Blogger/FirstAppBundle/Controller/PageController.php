<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace Blogger\FirstAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Import new namespaces
use Blogger\FirstAppBundle\Entity\Enquiry;
use Blogger\FirstAppBundle\Form\EnquiryType;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('BloggerFirstBundle:Page:index.html.twig');
    }
    
    public function aboutAction()
    {
        return $this->render('BloggerFirstBundle:Page:about.html.twig');
    }
    
    public function contactAction() {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

           if ($form->isValid()) {

                $message = \Swift_Message::newInstance()
                        ->setSubject('Contact enquiry from symblog')
                        ->setFrom('enquiries@symblog.co.uk')
                        ->setTo($this->container->getParameter('blogger_first.emails.contact_email'))
                        ->setBody($this->renderView('BloggerFirstBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->setFlash('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

                // Redirect - This is important to prevent users re-posting
                // the form if they refresh the page
                return $this->redirect($this->generateUrl('BloggerFirstBundle_contact'));
            }
        }

        return $this->render('BloggerFirstBundle:Page:contact.html.twig', array(
                    'form' => $form->createView()
                ));
    }
}